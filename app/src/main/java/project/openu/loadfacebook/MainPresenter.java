package project.openu.loadfacebook;

import android.util.Log;

import java.util.ArrayList;

import project.openu.loadfacebook.Model.Dtos;
import project.openu.loadfacebook.common.BaseListEntity;
import project.openu.loadfacebook.service.FacebookFriendList;
import project.openu.loadfacebook.service.NetworkClient;
import project.openu.loadfacebook.util.Constants;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by thanh on 02/04/2018.
 */

public class MainPresenter implements MainPresenterP {
    private MainView view;
    private boolean isLoadingMore = false;
    private ArrayList<Dtos> arrShipList = new ArrayList<>();

    private String TAG = "MainPresenter";

    public MainPresenter(MainView view) {
        this.view = view;

        view.initializeView();
    }

    @Override
    public void getShip60() {

//        getObservable().subscribe(getObserver());
        NetworkClient.getRetrofit().create(FacebookFriendList.class)
                .getShip60(String.valueOf(Constants.SKIP_SIZE), Constants.PAGE_SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BaseListEntity<Dtos>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "Completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "Error" + e);
                        e.printStackTrace();
                        view.displayError("Error fetching Movie Data");
                    }

                    @Override
                    public void onNext(BaseListEntity<Dtos> ship60BaseListEntity) {
                        isLoadingMore = false;
                        ArrayList<Dtos> newShipList = ship60BaseListEntity.getData();

                        //Get Correct insert Index
                        int index = 0;
                        if (arrShipList.size() > 0) {
                            index = arrShipList.size() - 1;
                        }
                        arrShipList.addAll(index, newShipList);


                        if ((Constants.TOP_SIZE > 0)) {
                            //Add Null object for loading more item
                            if (index == 0) {
                                arrShipList.add(null);
                            }
                        } else {
                            //Remove loading more item
                            index = arrShipList.size() - 1;
                            if (arrShipList.get(index) == null) {
                                arrShipList.remove(index);
                            }
                        }

                        view.loadShipList(arrShipList);
                    }
                });


    }

    @Override
    public void onLoadMore(int totalItemsCount, int visibleItemsCount, int firstVisibleItemPosition) {
        if (!isLoadingMore && (visibleItemsCount + firstVisibleItemPosition >= totalItemsCount)) {
            //loadMore
            isLoadingMore = true;
            Constants.SKIP_SIZE += 15;
            getShip60();
        }
    }

//    public Observable<Ship60> getObservable() {
//        return NetworkClient.getRetrofit().create(FacebookFriendList.class)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread());
//    }

//    public DisposableObserver<Ship60> getObserver() {
//        return new DisposableObserver<Ship60>() {
//            @Override
//            public void onNext(Ship60 ship60) {
//                isLoadingMore = false;
//                ArrayList<Dtos> newShipList = ship60.getDtos();
//
//                //Get Correct insert Index
//                int index = 0;
//                if (arrShipList.size() > 0) {
//                    index = arrShipList.size() - 1;
//                }
//                arrShipList.addAll(index, newShipList);
//
//
//                if ((Constants.TOP_SIZE > 0)) {
//                    //Add Null object for loading more item
//                    if (index == 0) {
//                        arrShipList.add(null);
//                    }
//                } else {
//                    //Remove loading more item
//                    index = arrShipList.size() - 1;
//                    if (arrShipList.get(index) == null) {
//                        arrShipList.remove(index);
//                    }
//                }
//
//                view.loadShipList(arrShipList);
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                Log.d(TAG, "Error" + e);
//                e.printStackTrace();
//                view.displayError("Error fetching Movie Data");
//            }
//
//            @Override
//            public void onComplete() {
//                Log.d(TAG, "Completed");
//            }
//        };
//    }

//    public DisposableObserver<MovieResponse> getObserver(){
//        return new DisposableObserver<MovieResponse>() {
//
//            @Override
//            public void onNext(@NonNull MovieResponse movieResponse) {
//                Log.d(TAG,"OnNext"+movieResponse.getTotalResults());
//                view.displayMovies(movieResponse);
//            }
//
//            @Override
//            public void onError(@NonNull Throwable e) {
//                Log.d(TAG,"Error"+e);
//                e.printStackTrace();
//                view.displayError("Error fetching Movie Data");
//            }
//
//            @Override
//            public void onComplete() {
//                Log.d(TAG,"Completed");
//                view.setSwipeRefreshLayout(false);
//            }
//        };
//    }


//    @Override
//    public void initialize() {
//        view.initializeView();
//        view.initializeFBSdk();
//        checkFBLoginStatus();
//    }
//
//    @Override
//    public void checkFBLoginStatus() {
//        AccessToken fbAccessToken = AccessToken.getCurrentAccessToken();
//        if (fbAccessToken != null) {
//            view.showFBLoginResult(fbAccessToken);
//        }
//    }
//
//    @Override
//    public void onShowFriendsListButtonClicked() {
//        view.showFriendsList();
//    }
//
//    @Override
//    public void onFBLoginSuccess(LoginResult loginResult) {
//        view.showFBLoginResult(loginResult.getAccessToken());
//    }
//
//    @Override
//    public void onLoginUsingFBManagerClicked() {
//        view.loginUsingFBManager();
//    }
}
