package project.openu.loadfacebook.common;

import com.google.gson.annotations.SerializedName;

/**
 * Created by thanh on 09/04/2018.
 */

public class BaseResponse {
    @SerializedName("Message")
    public String message;

    @SerializedName("ResponseCode")
    public int responseCode;
}
