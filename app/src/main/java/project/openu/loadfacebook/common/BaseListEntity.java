package project.openu.loadfacebook.common;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by thanh on 09/04/2018.
 */

public class BaseListEntity<T> extends BaseResponse {
    @SerializedName("Dtos")
    private ArrayList<T> data;

    public ArrayList<T> getData() {
        return data;
    }
}
