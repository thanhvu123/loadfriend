package project.openu.loadfacebook;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.ButterKnife;
import project.openu.loadfacebook.Model.Dtos;
import project.openu.loadfacebook.adapter.Ship60Adapter;
import project.openu.loadfacebook.util.Constants;

public class MainActivity extends AppCompatActivity implements MainView {

    RecyclerView rvShip;
    private String TAG = "MainActivity";
    Ship60Adapter shipAdapter;
    MainPresenter mainPresenter;
    private SwipeRefreshLayout swipeL;
    private ArrayList<Dtos> shipList;
//    ProgressBar progressBar;
//    SwipeRefreshLayout swipeL;

//    private MainPresenter mainPresenter;
//    private TextView tvLoginResult;
//    private Button btnShowFriendsList;
//    private LoginButton loginButton;
//    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        mainPresenter = new MainPresenter(this);
        mainPresenter.getShip60();

//        progressBar = (ProgressBar)findViewById(R.id.pb_loading);
//        progressBar.setVisibility(View.VISIBLE);
//        swipeL = (SwipeRefreshLayout)findViewById(R.id.swipe_lt);


//        mainPresenter = new MainPresenter(this);

//        loginButton = (LoginButton)findViewById(R.id.login_button);
//        callbackManager = CallbackManager.Factory.create();
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                txtView.setText("Login Success : " + "\n"
//                + loginResult.getAccessToken().getUserId() + "\n"
//                + loginResult.getAccessToken().getToken());
//                Log.d("AccessToken: " , loginResult.getAccessToken().getToken());
//            }
//
//            @Override
//            public void onCancel() {
//
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Log.d("Error", error.toString());
//            }
//        });
    }

    private void setOnClickSwipeRefresh() {
//        swipeL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                setSwipeRefreshLayout(true);
//                getMovieList();
//            }
//        });
    }


    @Override
    public void showToast(String str) {
        Toast.makeText(MainActivity.this,str,Toast.LENGTH_LONG).show();
    }


    @Override
    public void displayError(String e) {

        showToast(e);

    }

    @Override
    public void loadShipList(ArrayList<Dtos> arrayList) {
        shipList.removeAll(shipList);
        shipList.addAll(arrayList);
        swipeL.setRefreshing(false);

        if ((shipList != null) && (shipList.size() > 0)) {
            rvShip.setVisibility(View.VISIBLE);
            shipAdapter.notifyDataSetChanged();
        } else {
            rvShip.setVisibility(View.GONE);
        }
    }


    @Override
    public void initializeView() {
        Constants.SKIP_SIZE = 0;
        swipeL = (SwipeRefreshLayout) findViewById(R.id.swipe_lt);
        swipeL.setColorSchemeColors(
                getResources().getColor(android.R.color.holo_green_dark),
                getResources().getColor(android.R.color.holo_red_dark),
                getResources().getColor(android.R.color.holo_blue_dark),
                getResources().getColor(android.R.color.holo_orange_dark));
        swipeL.setRefreshing(true);

        swipeL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeL.setRefreshing(false);
            }
        });

        rvShip = (RecyclerView) findViewById(R.id.rvShip);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvShip.setLayoutManager(linearLayoutManager);

        shipList = new ArrayList<>();
        shipAdapter = new Ship60Adapter(MainActivity.this, shipList);
        rvShip.setAdapter(shipAdapter);

        rvShip.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemsCount = linearLayoutManager.getItemCount();
                int visibleItemsCount = rvShip.getChildCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                mainPresenter.onLoadMore(totalItemsCount, visibleItemsCount, firstVisibleItemPosition);
            }
        });
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//    }

//    @Override
//    public void initializeFBSdk() {
//        //init the callback manager
//        callbackManager = CallbackManager.Factory.create();
//        loginButton.registerCallback(callbackManager, fbLoginCallback);
//        LoginManager.getInstance().registerCallback(callbackManager, fbLoginCallback);
//    }
//
//    @Override
//    public void initializeView() {
//        tvLoginResult = (TextView) findViewById(R.id.tv_LoginResult);
//        loginButton = (LoginButton) findViewById(R.id.login_button);
//        btnShowFriendsList = (Button) findViewById(R.id.btn_showFriendsList);
//        btnShowFriendsList.setOnClickListener(this);
//    }
//
//    @Override
//    public void showFriendsList() {
//        Intent intent = new Intent(this, FriendsListActivity.class);
//        startActivity(intent);
//    }
//
//    @Override
//    public void showFBLoginResult(AccessToken fbAccessToken) {
//        btnShowFriendsList.setVisibility(View.VISIBLE);
//        tvLoginResult.setText(getString(R.string.success_login) + "\n" +
//                getString(R.string.user) + fbAccessToken.getUserId() + "\n" +
//                getString(R.string.token) + fbAccessToken.getToken()
//        );
//        Log.d("AccessToken", fbAccessToken.getToken());
//        Log.d("userId", fbAccessToken.getUserId());
//    }
//
//    @Override
//    public void loginUsingFBManager() {
//        //"user_friends" this will return only the common friends using this app
//        LoginManager.getInstance().logInWithReadPermissions(MainActivity.this,
//                Arrays.asList("public_profile", "user_friends", "email"));
//    }
//

//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.btn_showFriendsList:
//                mainPresenter.onShowFriendsListButtonClicked();
//                break;
//        }
//    }
//
//
//
//    private final FacebookCallback fbLoginCallback = new FacebookCallback<LoginResult>() {
//        @Override
//        public void onSuccess(LoginResult loginResult) {
//            mainPresenter.onFBLoginSuccess(loginResult);
//        }
//
//        @Override
//        public void onCancel() {
//
//        }
//
//        @Override
//        public void onError(FacebookException e) {
//
//        }
//    };
}
