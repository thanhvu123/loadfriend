package project.openu.loadfacebook;

import java.util.ArrayList;

import project.openu.loadfacebook.Model.Dtos;

/**
 * Created by thanh on 02/04/2018.
 */

public interface MainView {
//    void initializeFBSdk();
//
//    void initializeView();
//
//    void showFriendsList();
//
//    void showFBLoginResult(AccessToken fbAccessToken);
//
//    void loginUsingFBManager();

    void showToast(String s);


    void displayError(String s);

    void loadShipList(ArrayList<Dtos> arrayList);

    void initializeView();
}
