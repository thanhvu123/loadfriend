package project.openu.loadfacebook.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import project.openu.loadfacebook.Model.Dtos;
import project.openu.loadfacebook.R;

/**
 * Created by thanh on 05/04/2018.
 */

public class Ship60Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final int ITEM_TYPE_SHIP = 0;
    public final int ITEM_TYPE_LOAD = 1;
    private Context context;
    private ArrayList<Dtos> arrs;

    public Ship60Adapter(Context context, ArrayList<Dtos> arrs) {
        this.context = context;
        this.arrs = arrs;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == ITEM_TYPE_SHIP) {
            return new Ship60Holder(inflater.inflate(R.layout.item_ship60, parent, false));
        } else {
            return new LoadingViewHolder(inflater.inflate(R.layout.item_loading, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == ITEM_TYPE_SHIP) {
            Ship60Holder ship60Holder = ((Ship60Holder)holder);
            Dtos dtos = arrs.get(position);
            ship60Holder.tvFirstAddress.setText("First Address : " + dtos.getFirstPickupAddress());
            ship60Holder.tvLastAddress.setText("Last Address : " + dtos.getLastDeliveryAddress());
        }
    }

    @Override
    public int getItemCount() {
        return arrs.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (arrs.get(position) != null) {
            return ITEM_TYPE_SHIP;
        } else {
            return ITEM_TYPE_LOAD;
        }
    }

    public class Ship60Holder extends RecyclerView.ViewHolder {

        TextView tvFirstAddress, tvLastAddress;

        public Ship60Holder(View v) {
            super(v);
            tvFirstAddress = (TextView) v.findViewById(R.id.txtFirstAddress);
            tvLastAddress = (TextView) v.findViewById(R.id.txtLastAddress);
        }
    }
    public class LoadingViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar pbLoading;

        public LoadingViewHolder(View v) {
            super(v);
            pbLoading = (ProgressBar) v.findViewById(R.id.pb_loading);
        }
    }
}
