package project.openu.loadfacebook.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by thanh on 05/04/2018.
 */

public class Ship60 {
    @SerializedName("$id")
    private int id;
    @SerializedName("Dtos")
    private ArrayList<Dtos> dtos;
    @SerializedName("TotalRow")
    private int TotalRow;
    @SerializedName("ResponseCode")
    private int ResponseCode;
    @SerializedName("Message")
    private String Message;

    public Ship60() {
    }

    public Ship60(int id, ArrayList<Dtos> dtos, int totalRow, int responseCode, String message) {
        this.id = id;
        this.dtos = dtos;
        TotalRow = totalRow;
        ResponseCode = responseCode;
        Message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Dtos> getDtos() {
        return dtos;
    }

    public void setDtos(ArrayList<Dtos> dtos) {
        this.dtos = dtos;
    }

    public int getTotalRow() {
        return TotalRow;
    }

    public void setTotalRow(int totalRow) {
        TotalRow = totalRow;
    }

    public int getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(int responseCode) {
        ResponseCode = responseCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
