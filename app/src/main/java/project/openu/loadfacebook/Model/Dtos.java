package project.openu.loadfacebook.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by thanh on 05/04/2018.
 */

public class Dtos {
    @SerializedName("$id")
    private int id;
    @SerializedName("FirstPickupAddress")
    private String FirstPickupAddress;
    @SerializedName("LastDeliveryAddress")
    private String LastDeliveryAddress;

    public Dtos() {
    }

    public Dtos(int id, String firstPickupAddress, String lastDeliveryAddress) {
        this.id = id;
        FirstPickupAddress = firstPickupAddress;
        LastDeliveryAddress = lastDeliveryAddress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstPickupAddress() {
        return FirstPickupAddress;
    }

    public void setFirstPickupAddress(String firstPickupAddress) {
        FirstPickupAddress = firstPickupAddress;
    }

    public String getLastDeliveryAddress() {
        return LastDeliveryAddress;
    }

    public void setLastDeliveryAddress(String lastDeliveryAddress) {
        LastDeliveryAddress = lastDeliveryAddress;
    }
}
