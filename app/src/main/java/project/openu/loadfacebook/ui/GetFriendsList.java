package project.openu.loadfacebook.ui;

import project.openu.loadfacebook.Model.FriendsListResponse;
import project.openu.loadfacebook.service.ApiService;
import project.openu.loadfacebook.service.FacebookFriendList;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by thanh on 02/04/2018.
 */

public class GetFriendsList {
    public void getFBFriendsList(String userId, String accessToken, int limit, String afterPage, Callback<FriendsListResponse> friendsListCallback) {
        FacebookFriendList facebookListService = ApiService.getService().create(FacebookFriendList.class);
        Call<FriendsListResponse> call = facebookListService.getFriendsList(userId, accessToken, limit, afterPage);
        call.enqueue(friendsListCallback);
    }
}
