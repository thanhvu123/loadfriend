package project.openu.loadfacebook.ui.FriendsList;

import java.util.ArrayList;

import project.openu.loadfacebook.Model.FriendItemData;

/**
 * Created by thanh on 02/04/2018.
 */

public interface FriendsListView {
    void initializeView();

    void loadFriendsList(ArrayList<FriendItemData> friendsList);

    void showError();

    void onRefresh();
}
