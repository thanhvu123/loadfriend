package project.openu.loadfacebook.ui.FriendsList;

import android.util.Log;
import com.facebook.AccessToken;
import java.util.ArrayList;
import project.openu.loadfacebook.Model.FriendItemData;
import project.openu.loadfacebook.Model.FriendsListResponse;
import project.openu.loadfacebook.ui.GetFriendsList;
import project.openu.loadfacebook.util.Constants;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by thanh on 02/04/2018.
 */

public class FriendsListPresenter implements FriendListP {
    private String userId;
    private AccessToken fbToken;
    private FriendsListView view;
    private ArrayList<FriendItemData> friendsList = new ArrayList<FriendItemData>();
    private String nextPageId;
    private GetFriendsList friendsListUseCase = new GetFriendsList();
    private String previousPageId;
    private boolean isLoadingMore = false;

    public FriendsListPresenter(FriendsListView view) {
        this.view = view;
        view.initializeView();
    }

    @Override
    public void onGetFBFriendsList() {
        fbToken = AccessToken.getCurrentAccessToken();
        if(fbToken != null) {
            userId = fbToken.getUserId();
            friendsListUseCase.getFBFriendsList(userId, fbToken.getToken(), Constants.PAGE_SIZE, nextPageId, friendsListCallback);
        }else {
            view.showError();
        }
    }


    private final Callback<FriendsListResponse> friendsListCallback = new Callback<FriendsListResponse>() {
        @Override
        public void onResponse(Call<FriendsListResponse> call, retrofit2.Response<FriendsListResponse> response) {
            isLoadingMore = false;
            if (response.isSuccessful()) {
                FriendsListResponse responseResult = response.body();
                nextPageId = responseResult.getNextPageId();
                previousPageId = responseResult.getPreviousPageId();
                ArrayList<FriendItemData> newFriendsList = responseResult.getFriendsDataList();

                //Get Correct insert Index
                int index = 0;
                if (friendsList.size() > 0) {
                    index = friendsList.size() - 1;
                }
                friendsList.addAll(index, newFriendsList);

                if ((nextPageId != null)) {
                    //Add Null object for loading more item
                    if (index == 0) {
                        friendsList.add(null);
                    }
                } else {
                    //Remove loading more item
                    index = friendsList.size() - 1;
                    if (friendsList.get(index) == null) {
                        friendsList.remove(index);
                    }
                }

                view.loadFriendsList(friendsList);
            } else {
                //TODO show error message
            }
        }

        @Override
        public void onFailure(Call<FriendsListResponse> call, Throwable t) {
            isLoadingMore = false;
            Log.d("error", call.toString());
            //TODO Show error message
        }
    };

    @Override
    public void onLoadMore(int totalItemsCount, int visibleItemsCount, int firstVisibleItemPosition) {
        if ((nextPageId != null) && !isLoadingMore && (visibleItemsCount + firstVisibleItemPosition >= totalItemsCount)) {
            //loadMore
            isLoadingMore = true;
            friendsListUseCase.getFBFriendsList(userId, fbToken.getToken(), Constants.PAGE_SIZE, nextPageId, friendsListCallback);
        }
    }

    @Override
    public void onRefresh() {
        view.onRefresh();
    }
}
