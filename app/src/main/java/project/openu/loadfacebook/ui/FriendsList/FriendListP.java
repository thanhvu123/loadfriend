package project.openu.loadfacebook.ui.FriendsList;

/**
 * Created by thanh on 04/04/2018.
 */

public interface FriendListP {
    void onLoadMore(int totalItemsCount, int visibleItemsCount, int firstVisibleItemPosition);

    void onGetFBFriendsList();

    void onRefresh();
}
