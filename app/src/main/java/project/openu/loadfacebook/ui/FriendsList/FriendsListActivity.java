package project.openu.loadfacebook.ui.FriendsList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import project.openu.loadfacebook.Model.FriendItemData;
import project.openu.loadfacebook.R;
import project.openu.loadfacebook.adapter.FriendsAdapter;

/**
 * Created by thanh on 02/04/2018.
 */

public class FriendsListActivity extends AppCompatActivity implements FriendsListView {
    private ArrayList<FriendItemData> friendsList = new ArrayList<FriendItemData>();
    private SwipeRefreshLayout swipeLayout;
    private RecyclerView lvFriendsList;
    private FriendsAdapter friendsAdapter;
    private FriendsListPresenter presenter;
//    private Disposable disposable;
//    private CompositeDisposable mCompositeDisposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fbfriends_list);

        presenter = new FriendsListPresenter(this);
        presenter.onGetFBFriendsList();
//        createObservable();
//        mCompositeDisposable = new CompositeDisposable();
//        loadJSON();
    }

    @Override
    public void initializeView() {
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setColorSchemeColors(
                getResources().getColor(android.R.color.holo_green_dark),
                getResources().getColor(android.R.color.holo_red_dark),
                getResources().getColor(android.R.color.holo_blue_dark),
                getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setRefreshing(true);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(false);
            }
        });

        lvFriendsList = (RecyclerView) findViewById(R.id.rv_friends_list);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        lvFriendsList.setLayoutManager(linearLayoutManager);

        friendsAdapter = new FriendsAdapter(friendsList, FriendsListActivity.this);
        lvFriendsList.setAdapter(friendsAdapter);

        lvFriendsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemsCount = linearLayoutManager.getItemCount();
                int visibleItemsCount = lvFriendsList.getChildCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                presenter.onLoadMore(totalItemsCount, visibleItemsCount, firstVisibleItemPosition);
            }
        });
    }

    @Override
    public void loadFriendsList(ArrayList<FriendItemData> fLst) {
        friendsList.removeAll(friendsList);
        friendsList.addAll(fLst);
        swipeLayout.setRefreshing(false);

        if ((friendsList != null) && (friendsList.size() > 0)) {
            lvFriendsList.setVisibility(View.VISIBLE);
            friendsAdapter.notifyDataSetChanged();
        } else {
            lvFriendsList.setVisibility(View.GONE);
        }
    }

//    private void loadJSON() {
//
//        FacebookFriendList requestInterface = new Retrofit.Builder()
//                .baseUrl(ApiService.BASE_URL)
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())
//                .build().create(FacebookFriendList.class);
//
//        Disposable disposable = requestInterface.register(AccessToken.getCurrentAccessToken().getUserId(),
//                AccessToken.getCurrentAccessToken().getToken(), Constants.PAGE_SIZE, null)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(this::handleResponse, this::handleError, this::handleSuccess);
//
//        mCompositeDisposable.add(disposable);
//    }
//
//    private void handleResponse(List<FriendItemData> androidList) {
//
//        friendsList = new ArrayList<>(androidList);
//        friendsAdapter = new FriendsAdapter(friendsList, FriendsListActivity.this);
//        lvFriendsList.setAdapter(friendsAdapter);
//    }
//
//    private void handleError(Throwable error) {
//
//        Toast.makeText(this, "Error " + error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//    }
//
//    private void handleSuccess() {
//        Toast.makeText(this, "Get data success! ", Toast.LENGTH_SHORT).show();
//
//    }

//    private void createObservable() {
//        Observable<ArrayList<FriendItemData>> listObservable = Observable.just(friendsList);
//        disposable = listObservable.subscribe(colors -> friendsAdapter.setFriends(friendsList));
//    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.loginErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRefresh() {
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(false);
            }
        });
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        if (disposable != null && !disposable.isDisposed()) {
//            disposable.dispose();
//        }
//    }
}
