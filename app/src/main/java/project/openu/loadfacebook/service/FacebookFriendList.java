package project.openu.loadfacebook.service;

import java.util.List;

import project.openu.loadfacebook.Model.Dtos;
import project.openu.loadfacebook.Model.FriendItemData;
import project.openu.loadfacebook.Model.FriendsListResponse;
import project.openu.loadfacebook.Model.MovieResponse;
import project.openu.loadfacebook.common.BaseListEntity;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by thanh on 02/04/2018.
 */

public interface FacebookFriendList {
    @GET("v2.4/{user_id}/taggable_friends")
    Call<FriendsListResponse> getFriendsList(@Path("user_id") String userId,
                                             @Query("access_token") String accessToken,
                                             @Query("limit") int limit,
                                             @Query("after") String afterPage);

    @GET("v2.4/{user_id}/taggable_friends")
    Observable<List<FriendItemData>> register(@Path("user_id") String userId,
                                              @Query("access_token") String accessToken,
                                              @Query("limit") int limit,
                                              @Query("after") String afterPage);

    @GET("discover/movie")
    Observable<MovieResponse> getMovies(@Query("api_key") String api_key);

    @GET("groupshipments/simplify?$filter=Shippers/any(s: s/UserId eq 602 and s/IsSelected eq true) and (Status eq 'PickupUnsuccessful' or Status eq 'Completed' or  Status eq 'Canceled' or  Status eq 'Returned' or  Status eq 'SendUnsuccessful')&$orderby=CreatedTime desc")
    Observable<BaseListEntity<Dtos>> getShip60(@Query("$skip") String skip_number,
                                               @Query("$top") int top_number);
}
